// Phone stuff
let phoneinput = document.getElementById('phone')
let phoneButton = document.getElementById('callButton')
let phoneText = document.getElementById('phoneText')

function validate(evt) {
  var theEvent = evt || window.event;
  if (theEvent.type === 'paste') {
    key = event.clipboardData.getData('text/plain');
  }
  else {
    var key = theEvent.keyCode || theEvent.which
    key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if ( !regex.test(key) ) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}
phoneButton.addEventListener('click',(evt)=>{
  phoneinput.value = ''
  phoneText.innerHTML = "All distractions will be taken care of. Have a nice day and see you on the 14th!"
})

// Taxi stuff
let play = document.getElementById('taxiStart')

play.addEventListener('click',(evt)=>{
  document.body.style.backgroundColor = "black"
  var h2 = document.getElementsByTagName('h2')
  for (var i = 0; i < h2.length; i++) {
    h2[i].style.color = "white"
  }
  var h4 = document.getElementsByTagName('h4')
  for (var i = 0; i < h4.length; i++) {
    h4[i].style.color = "white"
  }
  var p = document.getElementsByTagName('p')
  for (var i = 0; i < p.length; i++) {
    p[i].style.color = "white"
  }
  var label = document.getElementsByTagName('label')
  for (var i = 0; i < label.length; i++) {
    label[i].style.color = "white"
  }
})

// Emotional stuff
let emotionButton = document.getElementById('feelsButton')
let userInput = document.getElementById('input')
let ourRespose = document.getElementById('emotionalResponse')
emotionButton.addEventListener('click',(evt)=>{
  userInput.value = ''
  ourRespose.innerHTML = "Ha, Git Good Kid!!!"
})



// Food Delivery input

let input1 = document.querySelector("#i1");
let input2 = document.querySelector("#i2");
let input3 = document.querySelector("#i3");
let input4 = document.querySelector("#i4");
let input5 = document.querySelector("#i5");
let input6 = document.querySelector("#i6");
let input7 = document.querySelector("#i7");

let total = document.querySelector("h4");
let num = 0;

// input1
input1.addEventListener('click', () => {
  console.log(input1.checked);
  if (input1.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})

// input 2
input2.addEventListener('click', () => {
  console.log(input2.checked);
  if (input2.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})

// input 3
input3.addEventListener('click', () => {
  console.log(input3.checked);
  if (input3.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})

// input 4
input4.addEventListener('click', () => {
  console.log(input4.checked);
  if (input4.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})

// input 5
input5.addEventListener('click', () => {
  console.log(input5.checked);
  if (input5.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})

// input 6
input6.addEventListener('click', () => {
  console.log(input6.checked);
  if (input6.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})

// input 7
input7.addEventListener('click', () => {
  console.log(input7.checked);
  if (input7.checked) {
    num += 13;
  } else {
    num -= 13;
  }
  console.log(num);
  total.textContent = `Total: $${num}`
})
